package hortonworks.hdp.refapp.trucking.storm.bolt.websocket;
import hortonworks.hdp.refapp.trucking.domain.VehicleData;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.IRichBolt;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Tuple;
import com.fasterxml.jackson.core.JsonProcessingException;

public class TransitWebSocketBolt implements IRichBolt {

	// ActiveMQ messages will expire after 10 seconds
	private static final long ACTIVEMQ_MESSAGE_TTL = 10000;
	private static final long serialVersionUID = -5319490672681173657L;
	private static final Logger LOG = LoggerFactory.getLogger(WebSocketBolt.class);

	private OutputCollector collector;
	private Properties config;
	private String user;
	private String password;
	private String activeMQConnectionString;
	private String topicName;

	private boolean sendAllEventsToTopic;
	private String allEventsTopicName;

	private Session session = null;
	private Connection connection = null;
	private ActiveMQConnectionFactory connectionFactory = null;

	private HashMap<String, MessageProducer> producers = new HashMap<String, MessageProducer>();

	public TransitWebSocketBolt(Properties config) {
		this.config = config;
	}

	@Override
	public void prepare(Map stormConf, TopologyContext context,
			OutputCollector collector) {
		this.collector = collector;
		this.user = config.getProperty("transit.notification.topic.user");
		this.password = config
				.getProperty("transit.notification.topic.password");
		this.activeMQConnectionString = config
				.getProperty("transit.notification.topic.connection.url");
		try {
			connectionFactory = new ActiveMQConnectionFactory(user, password,
					activeMQConnectionString);
			connection = connectionFactory.createConnection();
			connection.start();
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

		} catch (JMSException e) {
			LOG.error("Error sending TransitEvent to topic", e);
			return;
		}
	}

	@Override
	public void execute(Tuple input) {
		
                LOG.info("About to process tuple["+input+"]");
		 String postionData = input.getString(0);
		 
		 try {
			sendEventToTopic(postionData);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      collector.ack(input);  
		 
}	
private void sendEventToTopic(String postionData) throws JSONException, JMSException, JsonProcessingException {
		
	     JSONObject obj = new JSONObject(postionData);
	     JSONArray geodata = obj.getJSONArray("items");
	     int n = geodata.length();
	     
	     for (int i = 0; i < n; ++i) {
	    	 
	      JSONObject position = geodata.getJSONObject(i);
	      
	      long seconds_since_reports= position.getLong("seconds_since_report");
	      String run_id = position.getString("run_id");
	      double longitude=position.getDouble("longitude");
	      double heading=position.getDouble("heading");
	      long route_id=position.getLong("route_id");
	      boolean predictable=position.getBoolean("predictable");
	      double latitude=position.getDouble("latitude");
	      long id=position.getLong("id");
	  	  
              this.topicName = Long.toString(route_id);
	      VehicleData vehiceldata = new VehicleData(seconds_since_reports, run_id, longitude, heading, route_id, predictable, latitude,id);

	  	  if (producers.containsKey(topicName)) {
				LOG.info("Tpoic Already Created");
			} else {
				producers.put(topicName, getTopicProducer(session, this.topicName));
				LOG.info("Tpoic Created");
			}
	  	  constructEvent(vehiceldata);
	 
	     }
			
}
private MessageProducer getTopicProducer(Session session, String topic) {
		try {
			Topic topicDestination = session.createTopic(topic);
			MessageProducer topicProducer = session
					.createProducer(topicDestination);
			topicProducer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
			return topicProducer;
		} catch (JMSException e) {
			//LOG.error("Error creating producer for topic", e);
			throw new RuntimeException("Error creating producer for topic");
		}
	}
private void constructEvent(VehicleData vehiceldata) throws JMSException {
		    ObjectMapper mapper = new ObjectMapper();
		    String event = null;
		 
			try {
				event = mapper.writeValueAsString(vehiceldata);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		        TextMessage message = session.createTextMessage(event);
			MessageProducer producer = producers.get(this.topicName);
			producer.send(message, producer.getDeliveryMode(),
			producer.getPriority(), ACTIVEMQ_MESSAGE_TTL);
}
	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		// TODO Auto-generated method stub

	}

	@Override
	public Map<String, Object> getComponentConfiguration() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void cleanup() {
		// Todo
	}

}