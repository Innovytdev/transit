package hortonworks.hdp.refapp.trucking.domain;

import java.io.Serializable;

public class VehicleData implements Serializable {

	private long seconds_since_reports;
	private String run_id;
	private double longitude;
	private double heading;
	private long route_id;
	private boolean predictable;
	private double latitude;
	private long id;

	public VehicleData(long seconds_since_reports, String run_id,
			double longitude, double heading, long route_id,
			boolean predictable, double latitude, long id) {
		this.seconds_since_reports = seconds_since_reports;
		this.run_id = run_id;
		this.longitude = longitude;
		this.heading = heading;
		this.route_id = route_id;
		this.predictable = predictable;
		this.latitude = latitude;
		this.id = id;
	}

	public long getSeconds_since_reports() {
		return seconds_since_reports;
	}

	public void setSeconds_since_reports(long seconds_since_reports) {
		this.seconds_since_reports = seconds_since_reports;
	}

	public String getRun_id() {
		return run_id;
	}

	public void setRun_id(String run_id) {
		this.run_id = run_id;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getHeading() {
		return heading;
	}

	public void setHeading(double heading) {
		this.heading = heading;
	}

	public long getRoute_id() {
		return route_id;
	}

	public void setRoute_id(long route_id) {
		this.route_id = route_id;
	}

	public boolean isPredictable() {
		return predictable;
	}

	public void setPredictable(boolean predictable) {
		this.predictable = predictable;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "VehicleData [seconds_since_reports=" + seconds_since_reports
				+ ", run_id=" + run_id + ", longitude=" + longitude
				+ ", heading=" + heading + ", route_id=" + route_id
				+ ", predictable=" + predictable + ", latitude=" + latitude
				+ ", id=" + id + "]";
	}
}
