package hortonworks.hdp.refapp.trucking.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HConnectionManager;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.util.Bytes;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import hortonworks.hdp.refapp.trucking.model.Stop;

import org.slf4j.Logger;

import org.slf4j.LoggerFactory;

@Controller
public class LametroBusRoutesService {

	protected static Connection conn = null;
	protected static transient Configuration hbaseConf;
	protected static String zkQuorum = "10.10.1.144";
	protected static String zkPort = "2181";
	protected static String zkNodeParent = "/hbase-unsecure";
	protected static final String LAMETRO_ROUTE_STOP_TABLE = "route_stop_sequence";
	protected static String LAMETRO_STOP_COLUMN_FAMILY_NAME = "stop_info";
	private static final Logger LOG = LoggerFactory
			.getLogger(LametroBusRoutesService.class);

	@RequestMapping(value = "/routes/route/{route_id}", method = RequestMethod.GET)
	public @ResponseBody List<Stop> getRouteStopes(
			@PathVariable("route_id") String route_id) throws IOException,
			JSONException {

		System.out.println(">>>> Calling getRouteStopes route_id:" + route_id);

		String routeId = route_id;
		Configuration config = constructConfiguration();
		HConnection connection = HConnectionManager.createConnection(config);
		HTableInterface routeStopTable = connection
				.getTable(LAMETRO_ROUTE_STOP_TABLE);
		// JSONObject data= new JSONObject();

		Result res = doGet(routeStopTable, routeId);
		String stopSequence = Bytes.toString(res.getValue(
				Bytes.toBytes(LAMETRO_STOP_COLUMN_FAMILY_NAME),
				Bytes.toBytes("stop_sequence")));
		System.out.println("stop is :" + stopSequence);
		List<Stop> stops = getStopList(stopSequence);

		/*
		 * [ {"id":id,"longitude":longitude,"latitude":latitude,"display_name":
		 * display_name},
		 * {"id":id,"longitude":longitude,"latitude":latitude,"display_name"
		 * :display_name} ]
		 */

		return stops;
	}

	private List<Stop> getStopList(String stopSequence) throws JSONException {
		LOG.info("getStopList :" + stopSequence);
		// HashMap<String, String> stopMap = new HashMap<String, String>();
		List<Stop> stopList = new ArrayList<Stop>();

		JSONObject obj = new JSONObject(stopSequence);
		JSONArray geodata = obj.getJSONArray("items");
		int n = geodata.length();
		LOG.info("Array length is" + n);
		for (int i = 0; i < n - 1; ++i) {
			JSONObject stop = geodata.getJSONObject(i);

			Stop st = new Stop();

			long id = stop.getLong("id");
			double longitude = stop.getDouble("longitude");
			double latitude = stop.getDouble("latitude");
			String display_name = stop.getString("display_name");

			LOG.info("Display_name:" + display_name);

			st.setLatitude(latitude);
			st.setDisplay_name(display_name);
			st.setId(id);
			st.setLongitude(longitude);
			LOG.info("Data is " + st);

			stopList.add(st);
		}
		return stopList;

	}

	private Configuration constructConfiguration() {
		Configuration config = new Configuration();
		config.set("hbase.zookeeper.quorum", zkQuorum);
		config.set("hbase.zookeeper.property.clientPort", zkPort);
		config.set("zookeeper.znode.parent", zkNodeParent);
		config.set("hbase.defaults.for.version.skip", "true");
		return config;
	}

	private Result doGet(HTableInterface routeStopTable, String routeId) {

		Get g = new Get(Bytes.toBytes(routeId));

		try {
			return routeStopTable.get(g);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
